import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class driver {
    public class Node {
        private String text;
        public void setText(String t) { this.text = t; }
        public String getText() { return this.text; }

        private int pos;
        public void setPos(int p) { this.pos = p; }
        public int getPos() { return this.pos; }
    }

    //Where all of the data is kept
    Map<String, Node> mainData = new HashMap<String, Node>();
    //This is where we keep a list of all of the words we've encountered
    Set<String> alphaSorted = new TreeSet<String>();
    //This is where we'll keep a sorted
    Map<String, Integer> countSorted = new HashMap<String, Integer>();
    List<String> links = new LinkedList<String>();

    public static void main(String[] args) {
        driver dr = new driver();
        Document doc = null;

        File f = new File(URLEncoder.encode(args[0], StandardCharsets.UTF_8));
        while (!f.exists()) {
            dr.downloadWriteFile(args[0]);
        }

        try {
            doc = Jsoup.parse(f, "UTF-8", args[0]);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        //At this point, we're downloading the file successfully (if needed) and reading from the file.
        System.out.println(doc.body());





        //count how many times the word occurred in the document and the position of each occurrence of that word in the text.
        // Position is denoted by starting position in the text. Each position must also indicate whether that occurrence was capitalized or not.  Do not count HTML tags.
        //include a list of links that occur in the page. Do not include links on a page to itself.  This should be empty if the page has no links.

        //Read each word, compiling all data needed

        //Print first 15 words & counts in descending order by count.
    }

    public void downloadWriteFile(String urlStr) {
        try {
            File f = new File(URLEncoder.encode(urlStr, StandardCharsets.UTF_8));
            Writer fw = new FileWriter(f);
            URL url = new URL(urlStr);
            InputStream is = url.openStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            BufferedWriter writer = new BufferedWriter(fw);

            String line;

            while ((line= br.readLine()) != null) {
                writer.write(line);
                writer.newLine();
            }
            br.close();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void printTopByCount(Map<String, Map<String, Integer>> data, int count) {

    }
    public void printTopAlphabetically(Map<String, Map<String, Integer>> data, int count) {

    }
    public void printLinks(String links, int count) {
        System.out.print("None\n");
    }
}

